
# Title and Description

This project is all about getting the band's name which will play in the particular festivals .

This is a basic API skeleton written in Node Js. Very useful to building a front-end platforms like Android, iOS or JavaScript frameworks (Angular, Reactjs, etc).

This project will run on NodeJs using MongoDB as database. I have tried to maintain the code structure easy as any one can also adopt the flow and start building an API. Project is open for suggestions, Bug reports and pull requests.

## Features

- Pre-defined response structures with proper status codes.
- Included CORS.
- **FESTIVAL**  with **CRUD** operations.
- Validations added.
- Included API collection for [Postman](https://api.postman.com/collections/20435770-b57ccb0a-f8d0-4348-b13f-b5daad5271b8?access_key=PMAT-01GYW0BQEZMJF3BPH4Y4REGHKY).
- Light-weight project.
- Test cases with [JEST](https://jestjs.io/docs/getting-started) 
- Code coverage <npm run test:coverage>.
- Runs on multiple environments using DOCKER.

## Software Requirements

- Node Js Version 16 to LTS
- Docker LTS 
- Docker Compose LTS
- Mongo DB LTS (4 Version or above 4 Version)

## Features 

## How to install

### Using Git (recommended)

1.  Clone the project from github. Change "myproject" to your project name.

```bash
git clone https://gitlab.com/mma_zaki/festival-api ./myproject
```

### Using manual download ZIP

1.  Download repository
2.  Uncompress to your desired directory

### Install npm dependencies after installing (Git or manual download)

```bash
cd myproject
npm install

### Setting up environments

1.  You will find a file named `nodemon.jsone` on root directory of project.
2.  Create a new file by copying and pasting the file and then renaming it to just `.env`
    ```bash
    cp nodemon.json
    ```
3.  The file `.env` is already ignored, so you never commit your credentials.


## Project structure

```sh
.
├── app.js
├── package.json
├── config
│   └── logger.js
    ├── mongoConnect.js
│   └── mongoTest.js
├── controllers
│   ├── festivalController.js
├── models
│   ├── bands.js
│   └── mappings.js
    └── musicalFestival.js
├── routes
│   ├── festival.js
│   ├── index.js
├── test
│   ├── festivalService.test.js
├── logs
    ├── myapp.log
├── services
    ├── festivalServices.js

```

## How to run

### Running API server locally

```bash
npm run start
```

You will know server is running by checking the output of the command `npm run start`

```bash
Connected to Mongodb Connected
App is running ...

Press CTRL + C to stop the process.
```

**Note:** `MONGO_URI` will be your MongoDB connection string.

### Creating new models

If you need to add more models to the project just create a new file in `/models/` and use them in the controllers.

### Creating new routes

If you need to add more routes to the project just create a new file in `/routes/` and add it in `/routes/index.js` it will be loaded dynamically.

### Creating new controllers

If you need to add more controllers to the project just create a new file in `/controllers/` and use them in the routes.

## Tests

### Running Test Cases

```bash
npm test:coverage
```

You can set custom command for test at `package.json` file inside `scripts` property. You can also change timeout for each assertion with `--timeout` parameter of mocha command.

### Creating new tests

If you need to add more test cases to the project just create a new file in `/test/` and run the command.

### Running API server on any system

1. There should be docker install to all the system and docker compose .
2. In root folder there are two files Dockerfile and docker-compose.yml
3. run the cammand <docker build -t myapp .> it will create the image of the app.
4. After running creating image <docker-compose up -d> it will start the project 

 **Note:** All the cammand should be run on the root folder of the project.

## Logger

Proper logging is used 

1. Morgen is used for capturing http request and responce.
2. Wiston is used for logging errors.

## Postman 
- Postman collection link -- [Postman](https://api.postman.com/collections/20435770-b57ccb0a-f8d0-4348-b13f-b5daad5271b8?access_key=PMAT-01GYW0BQEZMJF3BPH4Y4REGHKY).