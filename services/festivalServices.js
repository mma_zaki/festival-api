const festivalModel = require("../models/musicalFestivals");
const bandsModel = require("../models/bands");
const mappingsModel = require("../models/mappings")
class servicesModel {
static async addBands(data){
    return bandsModel.create(data);
}
static async addFestival(data){
    return festivalModel.create(data);
}
static async addMappings(data){
    return mappingsModel.create(data);
}
static async getMappings(){
   
    return mappingsModel.find(
        {},
        {_id:0,createdAt:0,updatedAt:0}
      ).sort({"bands.name":1})
        .populate("bandId")
        .lean();
}
static async getFestivals(id){
    return festivalModel.findOne({_id:id},{}).sort({"name":1}).lean();
}
}
module.exports = servicesModel;