const servicesModel = require("../services/festivalServices");
const Logger = require("winston");
class festivalController {

    static async addBands(req,res) {
    try{
        let {name, recordLabel} = req.body;
        if(name && name!= null && name!='' && recordLabel && recordLabel!= null && recordLabel!='' ){
            let data = {
                name:name,
                recordLabel:recordLabel
            };
            let response = await servicesModel.addBands(data)
            return res.status(200).send(response);
        }else{
            return res.status(400).send("Plese check Entries again")
        }
    }
        catch(err){
            Logger.error({
                message: "error occured while adding Bands",
                error: err,
              });
              return res.status(500).send("Server Error");   
        }

    }
    static async addFestival(req,res) {
        try{
            let {name, } = req.body;
           
            if(name && name!= null && name!='' ){
                let data = {
                    name:name,
                    
                };
                let response = await servicesModel.addFestival(data)
                return res.status(200).send(response);
            }else{
                return res.status(400).send("Plese check Entries again")
            }
        }
            catch(err){
                Logger.error({
                    message: "error occured while adding festival",
                    error: err,
                  });
                  return res.status(500).send("Server Error");   
            }
    
        }
        static async addMappings(req,res) {
            try{
                let {recordLabel, bandId,festivalId} = req.body;
               
                if(recordLabel && recordLabel!= null && recordLabel!='' && bandId && bandId!= null && bandId!='' 
                && festivalId && festivalId.length != 0){
                    let data = {
                        recordLabel:recordLabel,
                        bandId:bandId,
                        festivalId:festivalId
                         };
                    let response = await servicesModel.addMappings(data)
                    return res.status(200).send(response);
                }else{
                    return res.status(400).send("Plese check Entries again")
                }
            }
                catch(err){
                    Logger.error({
                        message: "error occured while adding mappings",
                        error: err,
                      });
                      return res.status(500).send("Server Error");   
                }
        
            }
            static async getMappings(req,res) {
                try{
                    
                        let response = await servicesModel.getMappings();
                        
                       let dataCall = response.map(async (item)=>{
                       
                        let fest =[]
                        let bands = [];
                          let dataCallIn=  item.festivalId.map(async (data)=>{
                                
                                let festival = await servicesModel.getFestivals(data)
                              
                                fest.push({"name":festival.name})
                               
                                return data

                            })
                        
                            await Promise.all(dataCallIn)
                            bands.push({"name":item.bandId.name,"festivals":fest})
                           delete item.bandId;
                           delete item.festivalId;
                           delete item.__v;
                           item.bands = bands
                            return item
                        });
                        await Promise.all(dataCall)
                     
                        return res.status(200).send(response);
                   
                }
                    catch(err){
                        Logger.error({
                            message: "error occured while getting festival",
                            error: err,
                          });
                          return res.status(500).send("Server Error");   
                    }
            
                }
}
module.exports=festivalController;