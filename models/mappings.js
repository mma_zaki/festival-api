const mongoose = require("mongoose");

const Mappings = new mongoose.Schema(
    {
        recordLabel: { type: String, required: true },
        bandId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Band",
            default: null,
        },
        festivalId:  { type: mongoose.Schema.Types.Mixed, default: "" },
       
        },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("mappings", Mappings);
