const mongoose = require("mongoose");

const Bands = new mongoose.Schema(
    {
        name: { type: String, required: true },
        recordLabel: { type: String, required: true },
       
        },
    {
        timestamps: true,
    }
);

module.exports = mongoose.model("Band", Bands);
