const mongoose = require("mongoose");
const logger = require("winston");

mongoose.connect('mongodb+srv://root:13s257RuV04Iyf8d@majormongo-fc4b55b5.mongo.ondigitalocean.com/test?tls=true&authSource=admin&replicaSet=majormongo', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;

db.on("open", () => {
  logger.info("Mongodb Connected");
});

db.on("error", err => {
  logger.error({
    message: "Error in Connecting to Mongo",
    error: err
  });
});

module.exports = db;