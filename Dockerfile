FROM node:18-alpine

RUN apk add nano

WORKDIR /usr/src/app

COPY package.json .

RUN npm install --force
COPY . .

EXPOSE 8080

CMD ["npm","run","start"]