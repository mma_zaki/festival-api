const service = require('../../../services/festivalServices')

describe('Testing Services ', () => {
    let bands = {
        "name":"Band A test",
        "recordLabel":"Record Label ABS"
    };
    let festival= {
      "name":"test Festival"
       
  };
  let mappings = {
      "recordLabel":"Record Label 2 Test",
      "bandId":"64419f46843ba5efba5fdcea",
      "festi;valId":["64419c88ba0f9800ceda6806","64419cbaba0f9800ceda6808"]
       };
  let id ="64419c88ba0f9800ceda6806";
    beforeEach(async () => {
       
        require("../../../config/mongoTest");
    });

    test('Testing Add Bands', async () => {
      console.log("testing  all bands   ");
       service.addBands(bands) ;
   
    });
     test('Testing Add festival', async () => {
      console.log("testing  Add festival ");
     service.addFestival(festival) ;
   
     
   
    });
     test('Testing Add Mappings', async () => {
      console.log("testing  Add Mappings   ")
     service.addMappings(mappings) 
   
   
    });
    test('Testing get Mappings', async () => {
      console.log("testing  get Mappings  ")
     service.getMappings() 
  
   
    });
    test('Testing get Festival', async () => {
      console.log("testing  get Festival ")
     service.getFestivals(id) 
  
   
    });
});