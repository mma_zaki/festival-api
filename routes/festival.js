var express = require('express');
var router = express.Router();
const festivalController = require("../controller/festivalController")
/* GET home page. */
router.get('/festival', festivalController.getMappings);
router.post('/bands', festivalController.addBands);
router.post('/festival', festivalController.addFestival);
router.post('/mappings', festivalController.addMappings);

module.exports = router;
