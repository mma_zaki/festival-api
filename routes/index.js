var express = require('express');
var router = express.Router();
const api = require("./festival")
/* GET home page. */
router.use("/api/v1",api)
router.get("**", (req, res) => {
  return res.status(404).send("Page Not Found");
});

module.exports = router;
